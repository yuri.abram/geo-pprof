package modules

import (
	"gitlab/geo-pprof/internal/infrastructure/component"
	aservice "gitlab/geo-pprof/internal/modules/auth/service"
	gserv "gitlab/geo-pprof/internal/modules/geo/service"
)

type Services struct {
	GeoService gserv.GeoServicer
	Auth       aservice.Auther
}

func NewServices(storage *Storages, components *component.Components) *Services {
	return &Services{
		GeoService: gserv.NewGeoService(components.Logger, storage.Geo),
		Auth:       aservice.NewAuth(),
	}
}
