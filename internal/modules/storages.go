package modules

import (
	"gitlab/geo-pprof/internal/db/adapter"
	gstorage "gitlab/geo-pprof/internal/modules/geo/storage"
)

type Storages struct {
	Geo gstorage.GeoStorager
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		Geo: gstorage.NewGeoStorage(sqlAdapter),
	}
}
