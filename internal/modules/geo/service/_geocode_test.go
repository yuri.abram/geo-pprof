package service

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGeocodeAddress(t *testing.T) {

	ts := httptest.NewServer(http.HandlerFunc(Geocode))
	defer ts.Close()

	tests := []struct {
		name        string
		method      string
		requestBody string
		statusCode  int
		err         string
	}{
		{
			name:        "Valid POST request with coordinates",
			method:      http.MethodPost,
			requestBody: `{"lat": "40.7128", "lng": "-74.0059"}`, // Example coordinates
			statusCode:  http.StatusOK,
			err:         "",
		},
		{
			name:        "Invalid request method (GET)",
			method:      http.MethodGet,
			requestBody: "",
			statusCode:  http.StatusBadRequest,
			err:         "Bad request",
		},
		{
			name:        "Invalid JSON request body",
			method:      http.MethodPost,
			requestBody: "invalid JSON",
			statusCode:  http.StatusBadRequest,
			err:         "Error decoding request body:",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			req, err := http.NewRequest(tt.method, ts.URL, bytes.NewBufferString(tt.requestBody))
			if err != nil {
				t.Fatal(err)
			}

			w := httptest.NewRecorder()
			Geocode(w, req)

			assert.Equal(t, tt.statusCode, w.Code)

			if tt.err != "" {
				body := w.Body.String()
				assert.Contains(t, body, tt.err) // Assuming you use testify for assertions
			}
		})
	}
}
