package service

import (
	"context"
	"gitlab/geo-pprof/internal/models"
	"go.uber.org/zap"

	"gitlab/geo-pprof/internal/modules/geo/storage"
)

type GeoServicer interface {
	Search(ctx context.Context, query string) (models.GeoResponse, error)
}

type GeoService struct {
	logger  *zap.Logger
	storage storage.GeoStorager
}

func NewGeoService(logger *zap.Logger, storage storage.GeoStorager) *GeoService {
	return &GeoService{
		logger:  logger,
		storage: storage}
}

func (g *GeoService) Search(ctx context.Context, query string) (models.GeoResponse, error) {

	out, err := g.storage.Search(ctx, query)

	if err == nil && len(out.Addresses) > 0 {
		return out, nil
	}

	return out, nil
}
