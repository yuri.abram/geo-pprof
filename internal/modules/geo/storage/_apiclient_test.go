package storage

import (
	"github.com/stretchr/testify/assert"
	"gitlab/geo-pprof/internal/modules/geo/service"
	"net/http"
	"testing"
)

func Test_apiSelect(t *testing.T) {
	type args struct {
		reqType interface{}
	}
	tests := []struct {
		name         string
		args         args
		wantApiURL   string
		wantJsonBody []byte
		wantErr      bool
	}{
		{
			name: "test1",
			args: args{
				reqType: &service.SearchRequest{Query: "test"},
			},
			wantApiURL:   "http://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address",
			wantJsonBody: []byte(`{"query":"test"}`),
		},
		{
			name: "test2",
			args: args{
				reqType: &service.GeocodeRequest{Lat: "test1", Lng: "test2"},
			},
			wantApiURL:   "http://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address",
			wantJsonBody: []byte(`{"lat":"test1","lon":"test2"}`),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotApiURL, gotJsonBody, err := apiSelect(tt.args.reqType)
			assert.Equal(t, tt.wantApiURL, gotApiURL)
			assert.Equal(t, tt.wantJsonBody, gotJsonBody)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func Test_doRequest(t *testing.T) {
	type args struct {
		reqType interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    *http.Response
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test1",
			args: args{
				reqType: &service.SearchRequest{Query: ""},
			},
			want: &http.Response{
				StatusCode: 200,
			},
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, _ := DoRequest(tt.args.reqType)

			assert.Equal(t, tt.want.StatusCode, got.StatusCode)
		})
	}
}
