package modules

import (
	"gitlab/geo-pprof/internal/infrastructure/component"
	acontroller "gitlab/geo-pprof/internal/modules/auth/controller"
	gcontroller "gitlab/geo-pprof/internal/modules/geo/controller"
)

type Controllers struct {
	Geo  gcontroller.GeoController
	Auth acontroller.Auther
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)

	return &Controllers{
		Geo:  gcontroller.NewGeoCtl(services.GeoService, components),
		Auth: authController,
	}
}
