package router

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth/v5"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab/geo-pprof/docs"
	"gitlab/geo-pprof/internal/infrastructure/component"
	"gitlab/geo-pprof/internal/modules"
	"gitlab/geo-pprof/internal/modules/auth/service"
	"net/http"
	"net/http/pprof"
	_ "net/http/pprof"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()
	geocode := controllers.Geo
	authjwt := controllers.Auth

	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
	))

	// Protected routes
	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(service.TokenAuth))
		r.Use(jwtauth.Authenticator(service.TokenAuth))

		r.Post("/api/address/search", geocode.SearchResponding) //search.go

	})

	// Public routes
	r.Group(func(r chi.Router) {

		r.Post("/api/login", authjwt.Login)
		r.Post("/api/register", authjwt.Register)

	})

	// Debug routes, protected
	r.Group(func(r chi.Router) {
		//	r.Use(jwtauth.Verifier(service.TokenAuth))
		//	r.Use(jwtauth.Authenticator(service.TokenAuth))
		r.Route("/debug/pprof", func(r chi.Router) {
			r.HandleFunc("/", pprof.Index)
			r.HandleFunc("/cmdline", pprof.Cmdline)
			r.HandleFunc("/profile", pprof.Profile)
			r.HandleFunc("/symbol", pprof.Symbol)
			r.HandleFunc("/trace", pprof.Trace)

		})
	})

	return r
}
